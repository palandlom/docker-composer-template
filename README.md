# docker-composer-template

Запуск компосера:
```sh
docker-compose build
docker-compose up
```

Остановка и удаление контейнеров и другие ресурсы, созданные командой `docker-compose up`:
```sh
docker-compose down
```
Выводит журналы сервисов:
```sh
docker-compose logs -f [service name]
```

Вывести список запущенный процесс:
```sh
docker-compose ps
```

Выполнить команду в выполняющемся контейнере:
```sh
docker-compose exec [service name] [command]
```

Вывести список образов:
```sh
docker-compose images
```

Перестроить и запустить только один сервис/контейнер:
```sh
docker-compose up -d --no-deps --build <service_name>
```
--no-deps - Don't start linked services.

--build - Build images before starting containers.



docker-compose.yml
```
# Файл docker-compose должен начинаться с тега версии.
# Мы используем "3" так как это - самая свежая версия на момент написания этого кода.

version: "3"

# Следует учитывать, что docker-composes работает с сервисами.
# 1 сервис = 1 контейнер.
# Раздел, в котором будут описаны сервисы, начинается с 'services'.

services:

  # Первый сервис (контейнер) c именем сервер.
  my-server:
 
    # Ключевое слово "build" = путь к Dockerfile сервиса
    # Здесь 'server/' соответствует пути к папке сервера,
    # которая содержит соответствующий Dockerfile.
    build: server/

    # Команда, которую нужно запустить внутри образа после его запуска
    command: python ./server.py

    
    # Если мы хотим обратиться к серверу с нашего компьютера (находясь за пределами контейнера),
    # мы должны организовать перенаправление этого порта на порт компьютера.
    # Сделать это нам поможет ключевое слово 'ports'.
    # При его использовании применяется следующая конструкция: 
    # 	[порт компьютера]:[порт контейнера]

    ports:
      - 1234:1234

  # Второй сервис (контейнер): клиент.
  my-client:
    build: client/
    command: python ./client.py

    # Ключевое слово 'network_mode' используется для описания типа сети.
    # Тут мы указываем то, что контейнер может обращаться к 'localhost' компьютера.
    network_mode: host

    # Ключевое слово 'depends_on' позволяет указывать, должен ли сервис,
    # прежде чем запуститься, ждать, когда будут готовы к работе другие сервисы.
    # Нам нужно, чтобы сервис 'client' дождался бы готовности к работе сервиса 'server'.
    depends_on:
      - server
```



Dockerfile - [https://docs.google.com/document/d/1qff93BaynRzMyQKTwA6cZoUU67hH3CV7pFTs6Od0_Bc/edit?usp=sharing]

```sh
# our base image
# родительский образ
FROM python:3-onbuild
MAINTAINER Prakhar Srivastav <prakhar@prakhar.me>

# install system-wide deps for python and node
# запуск команд в контейнере при создании
RUN apt-get -yqq update
RUN apt-get -yqq install python-pip python-dev
RUN apt-get -yqq install nodejs npm
RUN ln -s /usr/bin/nodejs /usr/bin/node

# copy our application code
# добавление файлов с хоста в контейнер
ADD flask-app /opt/flask-app

# устанавливаем эту директорию в качестве рабочей, так что следующие 
# команды будут выполняться в контексте этой локации.
WORKDIR /opt/flask-app

# fetch app specific deps
RUN npm install

# И WORKDIR надо ставить перед ADD. Это ведь разные слои и в текущем порядке последний слой будет заново генерироваться при 
# каждом изменении файла проекта. Плюс прописывание команда только в композ файле, это не лучшее решение. Оставлять образ без 
# точки входа нельзя. Любой образ должен уметь запускаться командой start без допов.

```

```sh
# Dockerfile всегда должен начинаться с импорта базового образа.
# Для этого используется ключевое слово 'FROM'.
# Здесь нам нужно импортировать образ python (с DockerHub).
# В результате мы, в качестве имени образа, указываем 'python', а в качестве версии - 'latest'.

FROM python:latest

# Для того чтобы запустить в контейнере код, написанный на Python, нам нужно импортировать файлы 'server.py' и 'index.html'.
# Для того чтобы это сделать, мы используем ключевое слово 'ADD'.
# Первый параметр, 'server.py', представляет собой имя файла, хранящегося на компьютере.
# Второй параметр, '/server/', это путь, по которому нужно разместить указанный файл в образе.
# Здесь мы помещаем файл в папку образа '/server/'.

ADD server.py /server/
ADD index.html /server/

# Здесь мы воспользуемся командой 'WORKDIR', возможно, новой для вас.
# Она позволяет изменить рабочую директорию образа.
# В качестве такой директории, в которой будут выполняться все команды, мы устанавливаем '/server/'.

WORKDIR /server/
```

```sh
# То же самое, что и в серверном Dockerfile.

FROM python:latest

# Импортируем 'client.py' в папку '/client/'.

ADD client.py /client/

# Устанавливаем в качестве рабочей директории '/client/'.

WORKDIR /client/
```